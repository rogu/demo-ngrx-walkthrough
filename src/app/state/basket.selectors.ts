import { EntityState } from '@ngrx/entity';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CartItem, cartAdapter } from './basket.reducer';
import * as fromBooks from './books.selectors';

export const selectCartState =
  createFeatureSelector<EntityState<CartItem>>('basket');

export const { selectAll, selectTotal, selectEntities } = cartAdapter.getSelectors(selectCartState);

export const selectCart = createSelector(
  selectAll,
  fromBooks.selectEntities,
  (cart, booksEntities) =>
    cart.map((item) => ({ ...item, title: booksEntities[item.id]?.volumeInfo.title }))
);

export const selectCount = createSelector(
  selectAll,
  (state) => state.reduce((acc, item) => acc + item.count, 0)
);

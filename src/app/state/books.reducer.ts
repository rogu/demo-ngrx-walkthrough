import { createReducer, on } from "@ngrx/store";
import { fetchBooks, retrievedBookList } from "./books.actions";
import { Book, BooksState } from './books.model';

const state: BooksState<Book> = {
  loading: false,
  totalItems: 0,
  books: []
}

export const booksReducer = createReducer(
  state,
  on(fetchBooks, (state) => ({ ...state, loading: true })),
  on(retrievedBookList, (state, { books, totalItems }) => {
    return { ...state, books, totalItems, loading: false };
  })
);

import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Book, BooksState } from "./books.model";

export const selectBooksState = createFeatureSelector<BooksState<Book>>("books");

export const selectAll = createSelector(
  selectBooksState,
  (state) => { return state }
)

export const selectBooks = createSelector(
  selectBooksState,
  (state) => state.books
)

export const selectEntities = createSelector(
  selectBooksState,
  (state): { [key: string]: Book } => state.books.reduce((acc, book) => ({ ...acc, [book.id]: book }), {})
)

export const selectCount = createSelector(
  selectBooksState,
  (state) => state.totalItems
)

export const selectLoading = createSelector(
  selectBooksState,
  (state) => state.loading
)

export interface Book {
  id: string;
  volumeInfo: {
    title: string;
    authors: Array<string>;
  };
}
export interface BooksState<T> {
  books: T[],
  totalItems: number,
  loading: boolean
}

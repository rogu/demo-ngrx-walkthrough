import { CartItem } from '../state/basket.reducer';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html'
})
export class BasketComponent {
  @Input() items: ReadonlyArray<CartItem> = [];
  @Output() remove = new EventEmitter();
}
